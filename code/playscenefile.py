import pygame
import sys
import collections
from scenefile import Scene
from objectsfile import Floor, Rope
from characterfile import Character, CharacterRuda, CharacterMary
from textdisplayfile import textDisplay


class playScene(Scene):
	def __init__(self, ingame):
		Scene.__init__(self, ingame)
		self.tag = "play"

		self.firstFloorHeight = 100
		self.downtickV = 1
		self.downtickEvery = 2
		self.downtickCount = 0
		self.downtickA = 1
		self.downtickPeriod = 1000
		self.finished = False

		self.loadGraphics()
		self.initFloors()
		self.initCharacters()
	
	def initCharacters(self):
		self.ruda = CharacterRuda(self.floors[0].y1, self.game)
		self.mary = CharacterMary(self.floors[0].y1, self.game)

	def generateFloors(self):
		while True:
			temp = Floor(0, 0, 0, self.game)
			temp.makeNext(self.floors[len(self.floors) - 1])
			if temp.y1 >= 0:
				self.floors.append(temp)
			else:
				break

	def initFloors(self):
		self.floors = collections.deque()
		first = Floor(0, 0, 0, self.game)
		first.firstFloor(self.firstFloorHeight)
		self.floors.append(first)
		self.generateFloors()

	def loadGraphics(self):
		self.imgBackground = pygame.image.load("background.png")
		self.imgRope = pygame.image.load("rope.png")	
	
	def draw(self):
		self.game.screen.blit(self.imgBackground, (0, 0))
		for i in self.floors:
			i.display()
		self.ruda.display()
		self.mary.display()
		if self.ruda.alive == False or self.mary.alive == False:
			score = max(self.ruda.score, self.mary.score)
			scoreDisplay = textDisplay(str(score), self.game.sizeX/2, self.game.sizeY/2, self.game)
			scoreDisplay.display()
			self.game.currentScene = "pause"
			self.finished = True
		else:
			score = max(self.ruda.score, self.mary.score)
			scoreDisplay = textDisplay(str(score), 850, 40, self.game)
			scoreDisplay.display()

	def downtickAcceleration(self):
		self.downtickCount += 1
		if self.downtickCount % self.downtickPeriod == 0:
			self.downtickV += self.downtickA

	def downtick(self):
		self.downtickAcceleration()
		
		if self.downtickCount % self.downtickEvery == 0:
			for floor in self.floors:
				floor.downtick(self.downtickV)
		
		while len(self.floors) > 0 and self.floors[0].y1 >= self.game.sizeY:
			self.floors.popleft()		
		
		self.generateFloors()
		self.ruda.move()
		self.mary.move()

	def action(self):
		self.ruda.move()
		self.mary.move()
		self.downtick()
		self.draw()
