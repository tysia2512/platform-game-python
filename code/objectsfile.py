import pygame
import sys
import random
import time

class Floor():
	def __init__(self, coordinatesx, coordinatesy, length, ingame):
		self.x1 = coordinatesx #upper left corner
		self.y1 = coordinatesy
		self.blockSize = 16
		self.length = length * self.blockSize
		self.x2 = self.x1 + self.length
		self.y2 = self.y1 + self.blockSize
		self.level = 0
		
		random.seed(time.clock())
		self.displayType = random.randint(0, 12) + 1
		
		self.interval = 100
		self.margin = 5
		self.gap = 0
		self.minLength = 8
		
		self.game = ingame
		self.imgPlatformBegin = pygame.image.load("platforms/platformBegin"+str(self.displayType)+".png")
		self.imgPlatform = pygame.image.load("platforms/platform"+str(self.displayType)+".png")
		self.imgPlatformEnd = pygame.image.load("platforms/platformEnd"+str(self.displayType)+".png")


	def makeNext(self, previous):
		self.length = (self.minLength + random.randint(0, 10)) * self.blockSize
		self.y1 = previous.y1 - self.interval
		self.x1 = random.randint( max(self.margin, previous.x1 - self.length - self.gap), min(self.game.sizeX - self.margin - self.length, previous.x2 + self.gap) )
		self.x2 = self.x1 + self.length
		self.y2 = self.y1 + self.blockSize
		self.level = previous.level + 1

	def display(self):
		for i in range(self.length / self.blockSize):
			if i == 0:
				self.game.screen.blit(self.imgPlatformBegin, (self.x1 + i * self.blockSize, self.y1))
			elif i == self.length / self.blockSize - 1:
				self.game.screen.blit(self.imgPlatformEnd, (self.x1 + i * self.blockSize, self.y1))
			else:
				self.game.screen.blit(self.imgPlatform, (self.x1 + i * self.blockSize, self.y1))

	def firstFloor(self, height):
		self.x1 = 0
		self.y1 = self.game.sizeY - height - self.blockSize
		self.length = self.game.sizeX
		self.x2 = self.x1 + self.length
		self.y2 = self.y1 + self.blockSize
		self.level = 0

	def downtick(self, speed):
		self.y1 += speed
		self.y2 += speed

class Rope():
	def __init__(self, mary, ingame):
		self.nowUsed = False
		self.holder = mary
		self.game = ingame
		#self.length = self.game.play.floors[-1].interval
		self.length = 99
		self.blockSize = 3

		self.shiftLeft = 27
		self.shiftDown = 35

		self.imgRope = pygame.image.load("rope.png")
		self.update()

	def update(self):
		self.x1 = self.holder.x + self.shiftLeft
		self.y1 = self.holder.y + self.shiftDown
		self.x2 = self.x1 + self.blockSize
		self.y2 = self.y1 + self.length

	def use(self):
		self.nowUsed = True

	def display(self):
		self.update()
		if self.nowUsed:
			for i in range(self.length / self.blockSize):
				self.game.screen.blit(self.imgRope, (self.x1, self.y1 + i * self.blockSize))

	def intersect(self, ruda):
		if self.x2 >= ruda.x1 and self.x1 <= ruda.x2 and self.y2 >= ruda.y1 and self.y2 <= ruda.y2:
			return True
		return False



