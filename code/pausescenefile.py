import pygame
import sys
from scenefile import Scene
from playscenefile import playScene

class pauseScene(Scene):
	def __init__(self, ingame):
		Scene.__init__(self, ingame)
		self.tag = "pause"
	
	def action(self):
			self.game.play.draw()

			keys = pygame.key.get_pressed()
			if keys[pygame.K_SPACE]:
				self.game.currentScene = "play"
				if self.game.play.finished:
					self.game.play = playScene(self.game)