import pygame
import sys
import collections

class textDisplay():
	def __init__(self, text, x, y, ingame):
		self.x = x
		self.y = y
		self.text = text
		self.figures = []
		self.blockX = 29
		self.blockY = 42
		self.game = ingame
		
		for i in range(10):
			self.figures.append(pygame.image.load("figures/"+str(i)+".png"))
	
	def display(self):
		sizeX = len(self.text) * self.blockX
		sizeY = self.blockY

		startX = self.x - sizeX/2
		startY = self.y - sizeY/2

		for i in range(len(self.text)):
			self.game.screen.blit( self.figures[int(self.text[i])], (startX + i * self.blockX, startY) )


