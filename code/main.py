import pygame
import sys
from scenefile import Scene
from gamefile import Game
from menuscenefile import menuScene
from playscenefile import playScene
from pausescenefile import pauseScene

pygame.init()

xsize = 900
ysize = 1000
FPS = 30

game = Game(xsize, ysize, FPS)
game.run()

	