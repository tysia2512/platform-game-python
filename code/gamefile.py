import pygame
import sys
from scenefile import Scene
from menuscenefile import menuScene
from playscenefile import playScene
from pausescenefile import pauseScene

class Game():

	def __init__(self, mainWindowSizeX, mainWindowSizeY, fps):
		self.screen = pygame.display.set_mode((mainWindowSizeX, mainWindowSizeY), 0, 32)
		self.sizeX = mainWindowSizeX
		self.sizeY = mainWindowSizeY
		self.clock = pygame.time.Clock()
		self.currentScene = "pause"
		self.FPS = fps
		self.menu = menuScene(self)
		self.play = playScene(self)
		self.pause = pauseScene(self)

	def run(self):
		while True:
			
			if self.currentScene == "menu":
				if self.menu.run() == False:
					break
			
			if self.currentScene == "pause":
				if self.pause.run() == False:
					break

			if self.currentScene == "play":
				if self.play.run() == False:
					break

			pygame.display.flip()
			self.clock.tick(self.FPS)