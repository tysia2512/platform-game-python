import pygame
import sys

class Scene():

	def __init__(self, ingame):
		self.mainWindowSizeX = 900
		self.mainWindowSizeY = 1000
		self.tag = ""
		self.game = ingame

	def closeWindow(self):
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				sys.exit()
				return False
		return True
	
	def action(self):
		pass

	def run(self):
		while self.game.currentScene == self.tag:
			self.action()
			if self.closeWindow() == False:
				return False
			pygame.display.flip()
			self.game.clock.tick(self.game.FPS)
		return True
