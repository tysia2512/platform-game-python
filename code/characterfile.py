import pygame
import sys
import copy
from objectsfile import Floor, Rope

class Character():
	
	def __init__(self, y, ingame):
		#wymiary
		self.downCollider = 48
		self.upCollider = 3
		self.leftCollider = 8
		self.rightCollider = 41
		self.feetSize = 25
		self.headLeft = 13
		self.headRight = 36

		#pozycja
		self.score = 0
		self.y = y - self.downCollider
		self.game = ingame
		self.onRope = False
		self.ropeShift = -20
		self.alive = True
		
		#predkosci
		self.maxSideV = 5
		self.maxJumpV = 10
		self.sideV = 0
		self.jumpV = 0
		self.jumpA = 1
		self.sideA = 1

	def hideRope(self):
		pass

	def display(self):
		self.game.screen.blit(self.imgCharacter, (self.x, self.y) )

	def onPlatform(self):
		for floor in self.floors:
			if (self.x + self.feetSize >= floor.x1) and (self.x + self.feetSize <= floor.x2) and (self.y + self.downCollider == floor.y1):
				self.score = max(self.score, floor.level)
				return True
		return False

	def abovePlatform(self, floor):
		if (self.x + self.feetSize >= floor.x1) and (self.x + self.feetSize <= floor.x2) and (self.y + self.downCollider < floor.y1):
			return True
		return False

	def inPlatform(self, floor):
		if (self.x + self.feetSize >= floor.x1) and (self.x + self.feetSize <= floor.x2) and (self.y + self.downCollider > floor.y1) and (self.y + self.downCollider <= floor.y2):
			return True
		return False

	def detectCollision(self, future):
		for floor in self.floors:
			if future.inPlatform(floor) and self.abovePlatform(floor):
				self.y = floor.y1 - self.downCollider
				self.x = future.x - (self.sideV * (self.y - future.y) / self.jumpV)
				self.x = max(self.x, 0 - self.leftCollider)
				self.jumpV = 0
				self.x = min(self.x, self.game.sizeX - self.rightCollider)
				return
		self.x = future.x
		self.y = future.y

	def onKey(self):
		keys = pygame.key.get_pressed()
		
		if self.onRope:
			return

		if keys[self.rightKey] and keys[self.leftKey]:
			pass
		elif keys[self.rightKey]:
			self.sideV += self.sideA
			self.sideV = min(self.sideV, self.maxSideV)
		elif keys[self.leftKey]:
			self.sideV -= self.sideA
			self.sideV = max(self.sideV, -self.maxSideV)
		else:
			if self.sideV > 0:
				self.sideV -= self.sideA
				self.sideV = max(0, self.sideV)
			elif self.sideV < 0:
				self.sideV += self.sideA
				self.sideV = min(0, self.sideV)

		if keys[self.jumpKey] and self.onPlatform():
			self.jumpV = -self.maxJumpV
			self.jumpV = max(self.jumpV, -self.maxJumpV)
		elif not self.onPlatform():
			self.jumpV += self.jumpA
			self.jumpV = min(self.jumpV, self.maxJumpV)
		elif self.onPlatform():
			self.jumpV = 0

		self.hideRope()
		if keys[self.actionKey]:
			self.action()

	def update(self):
		pass
	
	def move(self):
		self.floors = copy.copy(self.game.play.floors)
		self.floors.append(self.game.play.ruda)
		if self.onRope:
			self.x = self.game.play.mary.rope.x1 + self.ropeShift
			self.update()
		if self.onRope and self.y <= self.game.play.mary.y:
			self.onRope = False
			self.jumpV = 0
		self.onKey()
		temp = copy.copy(self)
		temp.x += temp.sideV
		temp.x = max(temp.x, 0 - temp.leftCollider)
		temp.x = min(temp.x, temp.game.sizeX - temp.rightCollider)
		temp.y += temp.jumpV
		self.detectCollision(temp)
		self.update()
		self.onPlatform()
		if self.y >= self.game.sizeY:
			self.alive = False

class CharacterRuda(Character, Floor):

	def __init__(self, y, ingame):
		Character.__init__(self, y, ingame)
		Floor.__init__(self, 400, y, self.leftCollider - self.rightCollider, ingame)
		Character.__init__(self, y, ingame)
		self.x = 400
		self.imgCharacter = pygame.image.load("ruda.png")

		self.rightKey = pygame.K_d
		self.leftKey = pygame.K_a
		self.jumpKey = pygame.K_w
		self.actionKey = pygame.K_s
		self.update()

	def update(self):
		self.x1 = self.x + self.headLeft
		self.x2 = self.x + self.headRight
		self.y1 = self.y + self.upCollider
		self.y2 = self.y + self.downCollider

	def action(self):
		if self.game.play.mary.rope.intersect(self):
			self.x = self.game.play.mary.rope.x1 + self.ropeShift
			self.update()
			self.onRope = True
			self.jumpV = -self.maxJumpV
			self.sideV = 0


class CharacterMary(Character):

	def __init__(self, y, ingame):
		Character.__init__(self, y, ingame)
		self.x = 500
		self.imgCharacter = pygame.image.load("mary.png")
		
		self.rightKey = pygame.K_RIGHT
		self.leftKey = pygame.K_LEFT
		self.jumpKey = pygame.K_UP
		self.actionKey = pygame.K_DOWN

		self.rope = Rope(self, self.game)

	def display(self):
		self.game.screen.blit( self.imgCharacter, (self.x, self.y) )
		self.rope.display()

	def action(self):
		self.rope.use()

	def hideRope(self):
		self.rope.nowUsed = False